# BOM Builder v2

Re-designing the bom-builder with a better user experience and improved performance.


- [Mockup](https://nextcloud.kitspace.org/index.php/s/5t7yrZ7bAo5wBHd)

We will continue to track [issues on GitHub](https://github.com/kitspace/bom-builder/issues).

We will continue to use the [Partinfo server](https://github.com/kitspace/partinfo).


## Main Issues with v1

- Confusing user interaction
    - Open ended, user is not sure what to do
    - Quite a few user-interface "paper cuts" where functionality is hidden/confusing
- Performance issues rendering the table

## Links

- [BOM Builder v1 video](https://nextcloud.kitspace.org/index.php/s/NYZSHgNpW7TjmiZ)
- [BOM Builder v1 Pro (login required)](https://pro.kitspace.org/bom-builder/v1/)
- [BOM Builder v0.1 public](https://bom-builder.kitspace.org/)

## Evaluation of React spreadsheet libraries

### [react-datasheet](https://github.com/nadbm/react-datasheet) (current favorite)

- [Demo](https://nadbm.github.io/react-datasheet/)
- Branch: [datasheet](https://github.com/kitspace/bom-builder/blob/datasheet/src/table.js)
- Virtualization: no
- Issues:
    - No built-in virtualization
    - Uses "table" element by default
    - Ugly default styling

### [react-spreadsheet-grid](https://github.com/denisraslov/react-spreadsheet-grid)

- [Demo](https://denisraslov.github.io/grid/)
- Branch: [spreadsheet-grid](https://github.com/kitspace/bom-builder/blob/spreadsheet-grid/src/table.js)
- Virtualization: yes
- Issues:
    - No multi-select
    - [No move down/next on enter/tab](https://github.com/denisraslov/react-spreadsheet-grid/issues/36)
    - Weird focus API
    - [Some others](https://github.com/denisraslov/react-spreadsheet-grid/issues/created_by/kasbah)


### [react-data-grid](https://github.com/adazzle/react-data-grid)

- [Demo](https://adazzle.github.io/react-data-grid/docs/examples/default-editor)
- Branch: [data-grid](https://github.com/kitspace/bom-builder/blob/data-grid/src/table.js)
- Virtualization: yes
- Issues:
    - No multi-select
    - No move-down on enter
    - Drag down copy that can't be disabled
    - No "edit on single-click"
    - [Editable input moving out of place when window size adjusted](https://github.com/adazzle/react-data-grid/issues/1701)
